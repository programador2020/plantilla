/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plantilla;

/**
 *
 * @author sergio
 */
public class Plantilla {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
Crear el proyecto con Netbeans
Que sea de tipo Java/Java aplication
Declarar e inicializar las variables. Del tipo que corresponda, como texto o números.
Combinar (concatenar) las variables con el texto fijo.
Mostrarlo por pantalla.
         */
        String nombre = "Alejandra";
        int numero = 89000;
        String sexo;
        String saludo;
        String fecha = "12/04/2020";
        String hora = "10.00";
        String direccion = "San Martin 1200";
        String requisito1 = "Requisito 2";
        String requisito2 = "Requisito 2";
        String requisito3 = "Requisito 3";

        //H de hombre. M de mujer.
        sexo = "M";

        if (sexo.equals("M")) {
            //El caso verdadero (MUJER)            
            saludo = "Estimada";
        } else {
            //El caso falso (HOMBRE)
            if (sexo.equals("H")) {
                saludo = "Estimado";
            } else {
                saludo = "Estimado/a";
            }
        }

        //Primer parte del mensaje
        System.out.println(saludo + " " + nombre + ",");
        //Segunda parte del mensaje
        System.out.println("Confirmamos tu turno n°" + numero + " para realizar el trámite de Aptos Médicos. Debersá presentarte el " + fecha + " a las " + hora + " horas en " + direccion + " .");

        for (int i = 1; i < 3; i++) {
            System.out.println("Requisito: " + requisito1);
        }

    }

}
